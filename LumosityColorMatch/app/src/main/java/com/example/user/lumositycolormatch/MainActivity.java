package com.example.user.lumositycolormatch;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.w3c.dom.Text;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
@SuppressLint("NewApi")

public class MainActivity extends AppCompatActivity {

    private TextView leftTextView;
    private TextView rightTextView;
    private TextView correctAns;
    private TextView incorrectAns;
    private Button yesButton;
    private Button noButton;

    private ArrayList<Integer> colors;
    private ArrayList<String> colorNames;

    private int leftTextIndex;
    private int rightTextIndex;
    private int leftColorIndex;
    private int rightColorIndex;
    private int numberOfColors = 5;

    private int numberOftrueAns = 0;
    private int numberOffalseAns = 0;

    TextView textViewTime;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        leftTextView = (TextView) findViewById(R.id.leftTextView);
        rightTextView = (TextView) findViewById(R.id.rightTextView);

        correctAns = (TextView) findViewById(R.id.correctAns);
        incorrectAns = (TextView) findViewById(R.id.incorrectAns);

        yesButton = (Button) findViewById(R.id.yesButton);
        noButton = (Button) findViewById(R.id.noButton);

        textViewTime = (TextView) findViewById(R.id.textViewTime);

        start();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void start() {
        final CounterClass timer = new CounterClass(30000, 1000);
        timer.start();

        genData();

        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onYesClick();
            }
        });

        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onNoClick();
            }
        });
    }

    private void onYesClick() {
        if (leftTextIndex == rightColorIndex) {
            correct();
        } else {
            incorrect();
        }
        updateViews();
    }

    private void onNoClick() {
        if (leftTextIndex != rightColorIndex) {
            correct();
        } else {
            incorrect();
        }
        updateViews();
    }

    private void correct() {
        numberOftrueAns++;
        Snackbar.make(leftTextView, "Correct!", Snackbar.LENGTH_SHORT).show();
        correctAns.setText(String.valueOf(numberOftrueAns));
    }

    private void incorrect() {
        numberOffalseAns++;
        Snackbar.make(leftTextView, "Incorrect!", Snackbar.LENGTH_SHORT).show();
        incorrectAns.setText(String.valueOf(numberOffalseAns));
    }

    private void updateViews() {
        Random random = new Random();

        leftTextIndex = random.nextInt(numberOfColors);
        rightTextIndex = random.nextInt(numberOfColors);

        leftColorIndex = random.nextInt(numberOfColors);
        rightColorIndex = random.nextInt(numberOfColors);

        leftTextView.setText(colorNames.get(leftTextIndex));
        rightTextView.setText(colorNames.get(rightTextIndex));

        leftTextView.setBackgroundResource(colors.get(leftColorIndex));
        rightTextView.setBackgroundResource(colors.get(rightColorIndex));
    }

    private void genData() {
        colors = new ArrayList<>();
        colorNames = new ArrayList<>();

        colors.add(android.R.color.holo_blue_dark);
        colorNames.add("BLUE");

        colors.add(android.R.color.holo_red_dark);
        colorNames.add("RED");

        colors.add(android.R.color.black);
        colorNames.add("BLACK");

        colors.add(android.R.color.holo_green_dark);
        colorNames.add("GREEN");

        colors.add(android.R.color.holo_purple);
        colorNames.add("PURPLE");
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.user.lumositycolormatch/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.user.lumositycolormatch/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    @SuppressLint("NewApi")
    public class CounterClass extends CountDownTimer {

        public CounterClass(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @SuppressLint("NewApi")
        @TargetApi(Build.VERSION_CODES.GINGERBREAD)
        @Override
        public void onTick(long millisUntilFinished) {
            long millis = millisUntilFinished;
            String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                    TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                    TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
            textViewTime.setText(hms);

        }

        @Override
        public void onFinish() {
            showAlert();
        }
    }

    public void showAlert() {
        AlertDialog.Builder myAlert = new AlertDialog.Builder(this);
        myAlert.setMessage("Количество правильных ответов: " + numberOftrueAns + " и неправильных: " + numberOffalseAns);
        myAlert.setPositiveButton("Начать заново", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                start();
            }
        });
        myAlert.setTitle("Игра окончена!");
        myAlert.create();
        myAlert.show();
    }
}
